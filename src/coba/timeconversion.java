package coba;

import java.util.Scanner;

public class timeconversion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Waktu : ");
	    String time = input.next();
	    //Melakukan Splitting dari String time
	    String timeArr[] = time.split(":");
	    String AmPm = timeArr[2].substring(2,4);
	    int hh,mm,ss;
	    //mengubah String menjadi integer
	    hh = Integer.parseInt(timeArr[0]);
	    mm = Integer.parseInt(timeArr[1]);
	    ss = Integer.parseInt(timeArr[2].substring(0,2));
	        
	    String checkPM = "PM",checkAM ="AM";
	       
	    if(AmPm.equals(checkAM) && hh==12)
	    {
	        hh=0;
	    }
	    else if(AmPm.equals(checkPM)&& hh<12)
	    {
	        hh+=12;
	    } else if(AmPm.equals(checkPM)&& hh>12) {
	    	hh-=12;
	    }
	        
	    System.out.printf("%02d:%02d:%02d",hh,mm,ss);
	}

}

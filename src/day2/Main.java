package day2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";
		
		Scanner input = new Scanner(System.in);
		
		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case");
				int number = input.nextInt();
				
				switch(number) {
				case 1:
					Case01.resolve1();
					break;
				default:
					System.out.println("case is not available");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.out.println();
			System.out.println("Continue ? ");
			answer = input.nextLine();
		}
	}
	
}

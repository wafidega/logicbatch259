package day2;

import java.util.Iterator;
import java.util.Scanner;

public class latihanLooping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Nilai n = ");
		int n = input.nextInt();
		int x = 1;
		//Variabel Array
		int[] tempArrayInteger = new int[n]; // array untuk integer
		String[] tempStringInteger = new String[5]; //array untuk string
		
		//perulangan
//		for (int i = 0; i < n ; i++) {
//			//menggunakan kondisi
////			if (i % 2 == 0) {
////				System.out.print(i + " ");
////			}
//			
//			//menggunakan variabel k outputnya 1 1 1 1 1 1 1 1 1 1 
////			System.out.print(x + " ");
//			
//			//menggunakan variabel k outputnua ditambah hiji
////			System.out.print(x + " ");
////			x++;
//		}
		
		//Array
		for (int i = 0; i < tempArrayInteger.length; i++) {
			tempArrayInteger[i] = x;
			x++;
//			System.out.print(tempArrayInteger[i] + " "); Untuk mencetak output semua dalam array
		}
		
		for (int i = 0; i < tempArrayInteger.length; i++) {
			System.out.print(tempArrayInteger[i] + " ");
		}
	}

}

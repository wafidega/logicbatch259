package day2;

import java.util.Iterator;
import java.util.Scanner;

public class latihanNestedLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan Nilai N : ");
		int n = input.nextInt();
		System.out.println("Masukan Nilai N2 : ");
		int n2 = input.nextInt();
		//variabel x untuk berbagai soal
		int x = 0;
//		int x = 1;
//		int x = 3;
//		int x = 5;
		
		//variable y
//		int y = 1;
		int y = 0;
		
		//variable z
		int z = 0;
//		int z = y;
		
		
//		int [][] tempArray = new int[2][n]; // 2 = kolom , n = baris
		int [][] tempArray = new int[3][n];
		
		//soal 1
		
//		for (int i = 0; i < 2; i++) { //baris
//			for (int j = 0; j < n; j++) { //kolom
//				System.out.print(i + j + " ");
//				System.out.print((i + "," + j) + " ");
//				
//				if (i == 0) {
//					tempArray[i][j] = j;
//					System.out.print(tempArray[i][j] + " ");
//				} else {
//					tempArray[i][j] = x;
//					x*=n2;
//					System.out.print(tempArray[i][j] + " ");
//				}
//				
//			}
//			System.out.println();
//		}
		
		//soal 2
		
//		for (int i = 0; i < 2 ; i++) {
//			for (int j = 0; j < n ; j++) {
//				if (i==0) {
//					tempArray[i][j] = j;
//					System.out.print(tempArray[i][j] + " ");
//				} else if(j%3 == 2) {
//					tempArray[i][j] = (x*-1);
//					x*=n2;
//					System.out.print(tempArray[i][j] + " ");
//				} else {
//					tempArray[i][j] = x;
//					x*=n2;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
		
		//soal 3
		
//		for (int i = 0; i < 2 ; i++) {
//			for (int j = 0; j < n ; j++) {
//				if (i==0) {
//					tempArray[i][j] = j;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (j%3==1) {
//					tempArray[i][j] = (x/2);
//					System.out.print(tempArray[i][j] + " ");
//				} else {
//					tempArray[i][j] = (x);
//					x+=n2;
//					n2*=2;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
//		
		//soal 4 (output barisan akhir masih belum selesai)
		
//		for (int i = 0; i < 2 ; i++) {
//			for (int j = 0; j < n ; j++) {
//				if (i==0) {
//					tempArray[i][j] = j;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (j%2==0){
//					tempArray[i][j] = j;
//					System.out.print(tempArray[i][j] + " ");
//				} else {
//					tempArray[i][j] = x;
//					x+=n2;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
		
		//soal 5 dengan x = 0 (Selesai)
		
//		for (int i = 0; i < 3 ; i++) {
//			for (int j = 0; j < n ; j++) {
//				tempArray[i][j] = x;
//				x++;
//				System.out.print(tempArray[i][j] + " ");
//			}
//			System.out.println();
//		}
		
		// soal 6 dengan x = 0, y = 1 dan z = 1 (output baris 3 masih belum benar)
		
//		if (n==7) {
//			for (int i = 0; i < 3 ; i++) {
//				for (int j = 0; j < n ; j++) {
//					if (i==0) {
//						tempArray[i][j] = x;
//						x++;
//						System.out.print(tempArray[i][j] + " ");
//					} else if (i==1) {
//						tempArray[i][j] = y;
//						y*=7;
//						System.out.print(tempArray[i][j] + " ");
//					} else if (i==2) {
//						tempArray[i][j] = z;
//						z*=8;
//						System.out.print(tempArray[i][j] + " ");
//					}
//				}
//				System.out.println();
//			}
//		} else if(n == 5) {
//			for (int i = 0; i < 3 ; i++) {
//				for (int j = 0; j < n ; j++) {
//					if (i==0) {
//						tempArray[i][j] = x;
//						x++;
//						System.out.print(tempArray[i][j] + " ");
//					} else if (i==1) {
//						tempArray[i][j] = y;
//						y*=5;
//						System.out.print(tempArray[i][j] + " ");
//					} else if (i==2) {
//						tempArray[i][j] = z;
//						z*=6;
//						System.out.print(tempArray[i][j] + " ");
//					}
//				}
//				System.out.println();
//			}
//		}
		
		//soal 7 x = 0  ( Selesai)
		
//		for (int i = 0; i < 3 ; i++) {
//			for (int j = 0; j < n ; j++) {
//				tempArray[i][j] = x;
//				x++;
//				System.out.print(tempArray[i][j] + " ");
//			}
//			System.out.println();
//		}
		
		// soal no 8 dengan x = 0 , y = 0 dan z = 0
		
//		for ( int i = 0 ; i < 3 ; i++) {
//			for ( int j = 0; j < n ; j++) {
//				if (i == 0 )  {
//					tempArray[i][j] = x;
//					x++;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i == 1) {
//					tempArray[i][j] = y;
//					y+=2;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i==2) {
//					tempArray[i][j] = z;
//					z+=3;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
		
		// soal no 9 dengan menggunakan n2 dan x = 0 , y = 0 (output baris ke 3 masih belum sesuai)
//		for ( int i = 0 ; i < 3 ; i++) {
//			for ( int j = 0; j < n ; j++) {
//				if (i == 0 )  {
//					tempArray[i][j] = x;
//					x++;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i == 1) {
//					tempArray[i][j] = y;
//					y+=n2;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i==2) {
//					tempArray[i][j] = y;
//					y+=n2;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
		
		//soal 10 dengan menggunakan n2 dan x = 0 , y = 0 dan z = 0 ( Selesai)
//		for ( int i = 0 ; i < 3 ; i++) {
//			for ( int j = 0; j < n ; j++) {
//				if (i == 0 )  {
//					tempArray[i][j] = x;
//					x++;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i == 1) {
//					tempArray[i][j] = y;
//					y+=n2;
//					System.out.print(tempArray[i][j] + " ");
//				} else if (i==2) {
//					tempArray[i][j] = z;
//					z+=n2+1;
//					System.out.print(tempArray[i][j] + " ");
//				}
//			}
//			System.out.println();
//		}
		
		
	}

}

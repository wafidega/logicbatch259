package preTest;

import java.util.Scanner;

public class preTest6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		//Deklarasi 
		int pin = 123456;
		
		//deklarasi transfer
		int kodeBank = 001;
		int rekeningTujuan = 1234567890;
		
		System.out.println("Masukan Pin");
		int inputPin = input.nextInt();
		
		if(inputPin==pin) {
			
			//proses transaksi
			System.out.println("Masukan Uang Setor");
			int setor = input.nextInt();
			
			System.out.println("Pilihan Transfer");
			System.out.println("1. Antar Rekening");
			System.out.println("2. Antar Bank");
			int pilihan = input.nextInt();
			
			if(pilihan == 1) {
				System.out.println("Masukan Rekening Tujuan");
				int inputTujuan = input.nextInt();
				
				if(inputTujuan == rekeningTujuan && inputTujuan < 1000000000) {
					System.out.println("Masukan Nominal Tranfer");
					int nominal = input.nextInt();
					
					
					if(setor < nominal) {
						System.out.println("Maaf Saldo Anda Kurang");
					} else if (setor > nominal) {
						int totalTranferRekening = setor - nominal;
						System.out.println("Transaksi Berhasil Sisa Saldo Anda Asalah : " + totalTranferRekening);
					}
				} else {
					System.out.println("Nomor Tujuan Salah");
				}
			} else if (pilihan == 2) {
				System.out.println("Masukan Kode Bank : ");
				int inputKode = input.nextInt();
				
				if(inputKode == kodeBank) {
					System.out.println("Masukan Rekening Tujuan");
					int inputTujuan = input.nextInt();
					
					if(inputTujuan == rekeningTujuan) {
						
						System.out.println("Masukan Nominal Tranfer");
						int nominal = input.nextInt();
						
						
						if(setor < nominal) {
							System.out.println("Maaf Saldo Anda Kurang");
						} else if (setor > nominal) {
							int totalTranferRekening = setor - nominal - 7500;
							System.out.println("Transaksi Berhasil Sisa Saldo Anda Asalah : " + totalTranferRekening);
						}
					} else {
						System.out.println("Nomor Tujuan Salah");
					}
				} else {
					System.out.println("Kode bank Tidak ada di daftar");
				}
			}
			
		} else {
			System.out.println("PIN ANDA SALAH");
		}
	}

}

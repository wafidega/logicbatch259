package tugas2;

import java.util.Scanner;

public class tugas1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Input : ");
		int n = input.nextInt();
		
		// variable yang digunakan sebagai penampung bilangan fibonachi
		int fb[] = new int[n];
		
		// masukan nilai pada index array 
		fb[0] = 1; // menunjukan bahwa index 0 pada array fb adalah 1
        fb[1] = 1; // menunjukkan bahwa index 1 pada array fb adalah 1
        
        //melakukan perulangan untuk menampilkan deretan bilangan fibonachi
        for(int i = 2; i < n ; i++) { // menunjukka]n bahwa perulangan dimulai dari 2 kemudian dilakukan sebanyak n
            fb[i] = fb[i-1] + fb[i-2]; // melakukan perhitungan deretan bilangan fibonachi
            //fibonachi adalah deretan bilangan yang dimulai dari 0 dan 1 kemudian untuk selanjutnya menggunakan rumus (n-1) + (n-2) dan seterusnya
        }
         
        for (int i = 0; i < n ; i++) {
            System.out.print(fb[i] +  " "); //kemudian perulangan tersebut di cetak sebanyak inputan (n)
        }
		
		
	}

}

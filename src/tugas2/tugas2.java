package tugas2;

import java.util.Scanner;

public class tugas2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Input : ");
		int n = input.nextInt();
		
		int fibonaci[] = new int[n];
		
		fibonaci[0] = 1;
        fibonaci[1] = 1;
        fibonaci[2] = 1;
        
        for(int i = 3; i < n ; i++) {
            fibonaci[i] = fibonaci[i-1] + fibonaci[i-2] + fibonaci[i-3];
        }
         
        for (int i = 0; i < n ; i++) {
            System.out.print(fibonaci[i] +  " ");
        }
	}

}

package tugas4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class tugas1 {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Tanggal");
		String parkir = input.nextLine();
		
		System.out.println("Masukan Jam Masuk :");
		String jamMasuk = input.nextLine();
		
		System.out.println("Masukan Jam Keluar");
		String jamKeluar = input.nextLine();
		
		//menggunakan SimpleDateFormat
		SimpleDateFormat hari = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat jam = new SimpleDateFormat("hh:mm");
		
		//deklarasi jam masuk dan keluar parkir dengan nilai yang masih kosong
		Date jamMasukParkir = null;
		Date jamKeluarParkir = null;
		
		try {
			//Mengubah inputan masuk dan keluar menjadi format jam
			jamMasukParkir = jam.parse(jamMasuk);
			jamKeluarParkir = jam.parse(jamKeluar);
			//Menghitung selisih
			double selisih = jamMasukParkir.getTime() - jamKeluarParkir.getTime();
			double perubahan = selisih / (60*60*1000);
			int hasilParse = Integer.parseInt(String.valueOf(perubahan));
			int hasil = (int)Math.ceil(hasilParse);
			System.out.println(Math.abs(hasil));
		} 
		catch(Exception e) {
			e.printStackTrace();
			}
		
	}

}

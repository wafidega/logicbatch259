package tugas4;

import java.util.Arrays;
import java.util.Scanner;

public class tugas5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan panjang angka ");
		int n = input.nextInt();
		
		System.out.println("Masukan angka : ");
		double deretAngka[] = new double[n];
		
		for (int i = 0; i < n; i++) {
			double angka = input.nextDouble();
			deretAngka[i] = angka;
		}
		
		Arrays.sort(deretAngka);
		
		for(double hasilDeret : deretAngka ) {
			int konversiDeret = (int) hasilDeret;
			System.out.print(konversiDeret + " ");
		}
		
		System.out.println();
		
		//Menghitung rata-rata atau mean
		
		double jumlah = 0;
		for(int i = 0; i < n; i++) {
			jumlah += deretAngka[i];
		}
		
		System.out.println("Rata-ratanya adalah : " + (jumlah/n));
		
		// Menghitung median
		
		double hasilMedian = 0;
		
		if(n%2==1)
		{
			hasilMedian=deretAngka[((n+1)/2)-1];
			
		}
		else
		{
			hasilMedian=(deretAngka[n/2-1] + deretAngka[n/2])/2;
			
		}
	
	
	System.out.println("Median-nya adalah adalah : " + (hasilMedian));
		
		//Menghitung modus
	
		
		
		double nilaiMaksimal = 0; 
		
		int	maxModus = 0;
		 
        for (int i = 0; i < deretAngka.length; ++i) 
        {
            int modus = 0;
            for (int j = 0; j < deretAngka.length; ++j) 
            {
                if (deretAngka[j] == deretAngka[i])
                    ++modus;
            }
            if (modus > maxModus) 
            {
                maxModus = modus;
                nilaiMaksimal = deretAngka[i];
            }
        }
        System.out.println("Modusnya adalah : " + nilaiMaksimal);
	}

}

package tugasWeb;

import java.util.Scanner;

public class warmup3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan panjang array : ");
		int n = input.nextInt();
		System.out.println("Masukan Angka : ");
		int tempAngka[] = new int[n];
		
		for (int i = 0; i < n; i++) {
			int angka = input.nextInt();
			tempAngka[i] = angka;
		}
		
		int total = 0;
		
		for (int i = 0 ; i < tempAngka.length; i++) {
			total += tempAngka[i];
		}
		
		System.out.println("Hasilnya : " + total);
		
	}

}

package tugasWeb;

import java.util.Arrays;
import java.util.Scanner;

public class warmup4 { // DONEEEEE

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Masukan panjang : ");
		int n = input.nextInt();
		System.out.println("Masukan Angka : ");
		int tempAngka[][] = new int[n][n];
		int diagonal1 = 0;
		int diagonal2 = 0;
		
		for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
            	int angka = input.nextInt();
                tempAngka[i][j] = angka;
                if (i == j) {
                    diagonal1 = diagonal1 + tempAngka[i][j];
                }
                if (i + j == n - 1) {
                    diagonal2 = diagonal2 + tempAngka[i][j];
                }
            }
        }
		System.out.println();
        System.out.println("Hasil Pengurangan Diagonal adalah : ");
        System.out.println(Math.abs(diagonal1 - diagonal2));
		
	}

}
